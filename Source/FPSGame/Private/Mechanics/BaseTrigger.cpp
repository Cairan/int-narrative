// Copyright 2018 Disillusion Games. All Rights Reserved.

#include "BaseTrigger.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BillboardComponent.h"
#include "Components/ArrowComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "GameFramework/Character.h"


ABaseTrigger::ABaseTrigger() 
{
	RootTriggerBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("RootBoxComponent"));
	if (RootTriggerBoxComponent)
	{
		RootComponent = RootTriggerBoxComponent;
		RootTriggerBoxComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	}

	VisualStaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualStaticMeshComponent"));
	if (VisualStaticMeshComponent) {
		VisualStaticMeshComponent->SetupAttachment(RootComponent);
		VisualStaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		VisualStaticMeshComponent->bVisible = true;
		VisualStaticMeshComponent->SetHiddenInGame(true);

		if (MaterialInstance) {
			DynamicMaterial = UMaterialInstanceDynamic::Create(MaterialInstance, this);
			DynamicMaterial->SetVectorParameterValue("TriggerColor", TriggerColor);
			VisualStaticMeshComponent->SetMaterial(0, DynamicMaterial);
		}

	}

	IconBillboardComponent = CreateDefaultSubobject<UBillboardComponent>(TEXT("IconBillboardComponent"));
	if (IconBillboardComponent) {
		IconBillboardComponent->SetupAttachment(VisualStaticMeshComponent);
		IconBillboardComponent->bVisible = true;
		IconBillboardComponent->SetHiddenInGame(true);
	}

	ForwardDirectionArrowComponent = CreateDefaultSubobject<UArrowComponent>(TEXT("ForwardDirectionArrowComponent"));
	if (ForwardDirectionArrowComponent) {
		ForwardDirectionArrowComponent->SetupAttachment(RootComponent);
		ForwardDirectionArrowComponent->SetWorldScale3D(FVector(0.2f, 0.2f, 0.2f));
		
#if UE_EDITOR
		ForwardDirectionArrowComponent->bTreatAsASprite = true;
		ForwardDirectionArrowComponent->ArrowColor = FColor(150, 200, 255);
		ForwardDirectionArrowComponent->ArrowSize = 1.0f;
		ForwardDirectionArrowComponent->bIsScreenSizeScaled = true;
		ForwardDirectionArrowComponent->ArrowColor = FColor(150, 200, 255);
		UpdateTriggerDirectionArrow();
#endif
	}
}

void ABaseTrigger::BeginPlay() {

	Super::BeginPlay();

	if (!VisualStaticMeshComponent || !IconBillboardComponent) { return; }
	VisualStaticMeshComponent->SetHiddenInGame(!bDebug);
	IconBillboardComponent->SetHiddenInGame(!bDebug);
}

void ABaseTrigger::OnConstruction(const FTransform& Transform) {

	Super::OnConstruction(Transform);

	BindDelegates();
}

void ABaseTrigger::BindDelegates() {
	if (!RootTriggerBoxComponent) { return; }
	RootTriggerBoxComponent->OnComponentBeginOverlap.Clear();
	RootTriggerBoxComponent->OnComponentEndOverlap.Clear();
	RootTriggerBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ABaseTrigger::OnOverlapBegin);
	RootTriggerBoxComponent->OnComponentEndOverlap.AddDynamic(this, &ABaseTrigger::OnOverlapEnd);
}

void ABaseTrigger::HandleTriggerEnter(ACharacter* PlayerCharacter, AActor* TriggerReactingActor) {
	TriggeredAmount++;
	OnTriggerEnter(PlayerCharacter, TriggerReactingActor);
}

void ABaseTrigger::HandleTriggerExit(ACharacter* PlayerCharacter, AActor* TriggerReactingActor) {
	OnTriggerExit(PlayerCharacter, TriggerReactingActor);
}

bool ABaseTrigger::IsEnterDirectionValid(const FHitResult& SweepResult) {
	if (!bUseEnterDirection) { return true; }

	const FVector Direction = GetDirectionFromTriggerDirection();
	if (IsCorrectEnterDirection(Direction, SweepResult.ImpactNormal)) {
		return true;
	}

	return false;
}

bool ABaseTrigger::IsCorrectEnterDirection(FVector DirectionVector, const FVector_NetQuantizeNormal ImpactNormal) {
	const float Dot = FVector::DotProduct(DirectionVector, ImpactNormal);
	if (FMath::IsNearlyEqual(Dot, -1.0f, KINDA_SMALL_NUMBER)) {
		return true;
	}
	return false;
}

FVector ABaseTrigger::GetDirectionFromTriggerDirection() const {
	FVector Direction;

	switch (TriggerDirection) {
	case ETriggerDirection::TD_FRONT:
		Direction = GetActorForwardVector();
		break;
	case ETriggerDirection::TD_BACK:
		Direction = GetActorForwardVector() * -1;
		break;
	case ETriggerDirection::TD_LEFT:
		Direction = GetActorRightVector() * -1;
		break;
	case ETriggerDirection::TD_RIGHT:
		Direction = GetActorRightVector();
		break;
	case ETriggerDirection::TD_TOP:
		Direction = GetActorUpVector();
		break;
	}

	return Direction;
}

void ABaseTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	ACharacter* Character = Cast<ACharacter>(OtherActor);
	AActor* ReactingActor = OtherActor;

	if (!Character) {
		return;
	}

	if (!IsEnterDirectionValid(SweepResult)) { 
		return; 
	}

	const bool bTriggerCountExceeded = bUseMaxTriggerCount && CurrentTriggerCount >= MaxTriggerCount;
	if (bTriggerCountExceeded) { 
		return; 
	}
	
	CurrentTriggerCount++;
	HandleTriggerEnter(Character, ReactingActor);
}

void ABaseTrigger::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	ACharacter* Character = Cast<ACharacter>(OtherActor);
	AActor* ReactingActor = OtherActor;

	if (!Character) {
		return;
	}

	const bool bTriggerCountExceeded = bUseMaxTriggerCount && CurrentTriggerCount >= MaxTriggerCount;
	if (bTriggerCountExceeded) { 
		return; 
	}

	HandleTriggerExit(Character, ReactingActor);
}

//////////////////////////////////////////////////////////////////////////
// Blueprint Events

void ABaseTrigger::OnTriggerEnter_Implementation(ACharacter* PlayerCharacter, AActor* TriggerReactingActor) { }

void ABaseTrigger::OnTriggerExit_Implementation(ACharacter* PlayerCharacter, AActor* TriggerReactingActor) { }

//////////////////////////////////////////////////////////////////////////
// Editor Only

#if UE_EDITOR
void ABaseTrigger::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) {
	Super::PostEditChangeProperty(PropertyChangedEvent);

	UpdateTriggerDirectionArrow();

	if (!MaterialInstance) { return; }
	DynamicMaterial = UMaterialInstanceDynamic::Create(MaterialInstance, this);
	DynamicMaterial->SetVectorParameterValue("TriggerColor", TriggerColor);
	VisualStaticMeshComponent->SetMaterial(0, DynamicMaterial);
}

void ABaseTrigger::UpdateTriggerDirectionArrow() {
	if (!ForwardDirectionArrowComponent) { return; }
	if (!bUseEnterDirection) {
		ForwardDirectionArrowComponent->SetVisibility(false);
		return;
	}
	
	ForwardDirectionArrowComponent->SetVisibility(true);
	const FVector Direction = GetDirectionFromTriggerDirection();
	const FRotator Rotation = GetActorTransform().InverseTransformRotation(Direction.Rotation().Quaternion()).Rotator();
	ForwardDirectionArrowComponent->SetRelativeRotation(Rotation);
}
#endif

