// Copyright 2018 Disillusion Games. All Rights Reserved.

#include "CoreActor.h"


ACoreActor::ACoreActor()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
}