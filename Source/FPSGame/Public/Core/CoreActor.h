// Copyright 2018 Disillusion Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CoreActor.generated.h"

UCLASS()
class FPSGAME_API ACoreActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ACoreActor();

protected:
	UPROPERTY(Category = "CoreActor", EditAnywhere, BlueprintReadOnly)
	bool bDebug = false;
	
};
