// Copyright 2018 Disillusion Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Core/CoreActor.h"
#include "BaseTrigger.generated.h"


class UBoxComponent;
class UStaticMeshComponent;
class UBillboardComponent;
class UMaterialInstance;
class UMaterialInstanceDynamic;
class ACharacter;
class UArrowComponent;


UENUM(BlueprintType)
enum class ETriggerDirection : uint8
{
	TD_FRONT	UMETA(DisplayName = "Front"),
	TD_LEFT		UMETA(DisplayName = "Left"),
	TD_RIGHT	UMETA(DisplayName = "Right"),
	TD_BACK		UMETA(DisplayName = "Rear"),
	TD_TOP		UMETA(DisplayName = "Top")
};

/**
 * 
 */
UCLASS()
class FPSGAME_API ABaseTrigger : public ACoreActor
{
	GENERATED_BODY()
	
public:
	ABaseTrigger();

protected:
	UPROPERTY(Category = "BaseTrigger", VisibleDefaultsOnly, BlueprintReadOnly)
	UBoxComponent* RootTriggerBoxComponent;

	UPROPERTY(Category = "BaseTrigger", VisibleDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent* VisualStaticMeshComponent;

	UPROPERTY(Category = "BaseTrigger", VisibleDefaultsOnly, BlueprintReadOnly)
	UBillboardComponent* IconBillboardComponent;

	UPROPERTY(Category = "BaseTrigger", VisibleDefaultsOnly, BlueprintReadOnly)
	UArrowComponent* ForwardDirectionArrowComponent;

protected:
	UPROPERTY(Category = "BaseTrigger", EditAnywhere, BlueprintReadOnly)
	bool bUseMaxTriggerCount = false;

	/* The Amount of Time the trigger can be activated by the player */
	UPROPERTY(Category = "BaseTrigger", EditAnywhere, BlueprintReadOnly, meta = (EditCondition = bUseMaxTriggerCount, ClampMin = 0))
	int MaxTriggerCount = 0;

	/* Should this only be triggered when entered from a certain direction */
	UPROPERTY(Category = "BaseTrigger", EditAnywhere, BlueprintReadOnly)
	bool bUseEnterDirection = false;

	UPROPERTY(Category = "BaseTrigger", EditAnywhere, BlueprintReadOnly, AdvancedDisplay)
	TArray<TSubclassOf<AActor>> TriggerReactingActors;

	/* The direction the player has to enter the trigger */
	UPROPERTY(Category = "BaseTrigger", EditAnywhere, BlueprintReadOnly, meta = (EditCondition = bUseEnterDirection))
	ETriggerDirection TriggerDirection;

	/* The color of the trigger in the editor */
	UPROPERTY(Category = "BaseTrigger", EditAnywhere, BlueprintReadOnly, AdvancedDisplay, meta = (HideAlphaChannel))
	FLinearColor TriggerColor;

	/* The trigger material instance to use */
	UPROPERTY(Category = "BaseTrigger", EditAnywhere, BlueprintReadOnly, AdvancedDisplay)
	UMaterialInstance* MaterialInstance;

private:
	UMaterialInstanceDynamic* DynamicMaterial;
	int CurrentTriggerCount = 0;
	int TriggeredAmount = 0;

protected:
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void BindDelegates();
	virtual void HandleTriggerEnter(ACharacter* PlayerCharacter, AActor* TriggerReactingActor);
	virtual void HandleTriggerExit(ACharacter* PlayerCharacter, AActor* TriggerReactingActor);
	bool IsEnterDirectionValid(const FHitResult& SweepResult);
	bool IsCorrectEnterDirection(FVector DirectionVector, const FVector_NetQuantizeNormal ImpactNormal);
	FVector GetDirectionFromTriggerDirection() const;

public:
	UFUNCTION(Category = "BaseTrigger")
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(Category = "BaseTrigger")
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	// Blueprint Events
	UFUNCTION(Category = "BaseTrigger", BlueprintNativeEvent)
	void OnTriggerEnter(ACharacter* PlayerCharacter, AActor* TriggerReactingActor);

	UFUNCTION(Category = "BaseTrigger", BlueprintNativeEvent)
	void OnTriggerExit(ACharacter* PlayerCharacter, AActor* TriggerReactingActor);
	// ~Blueprint Event

protected:
#if UE_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	virtual void UpdateTriggerDirectionArrow();
#endif

};
